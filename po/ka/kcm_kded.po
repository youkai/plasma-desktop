# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-29 02:07+0000\n"
"PO-Revision-Date: 2023-06-29 05:54+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr "სერვისის გაჩერების შეცდომა: %1"

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr "სერვისის გაშვების შეცდომა: %1"

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr "სერვისის გაჩერების შეცდომა."

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr "სერვისის გაშვების შეცდომა."

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""
"KDE-ის სერვისების მმართველისთვის (kded6) შენახული ცვლილებების შესახებ "
"შეტყობინების გაგზავნის შეცდომა: %1"

#: ui/main.qml:36
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""
"ფონური სერვისების მმართველი (kded6) გაშვებული არაა. დარწმუნდით, რომ ის "
"სწორად აყენია."

#: ui/main.qml:45
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""
"თუ ისინი საჭირო არაა, ზოგიერთი სერვისი, როცა ხელით გაუშვებთ, თვითონ "
"გათიშავენ თავის თავს."

#: ui/main.qml:54
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""
"თქვენი ცვლილებების გადატარებისას ფონური სერვისების მმართველის (kded6) "
"გადატვირთვისას ზოგიერთი სხვა სერვისიც ავტომატურად გაჩერდა/ჩაირთო."

#: ui/main.qml:96
#, kde-format
msgid "All Services"
msgstr "ყველა სერვისი"

#: ui/main.qml:97
#, kde-format
msgctxt "List running services"
msgid "Running"
msgstr "გაშვებული"

#: ui/main.qml:98
#, kde-format
msgctxt "List not running services"
msgid "Not Running"
msgstr "გაშვებული არაა"

#: ui/main.qml:134
#, kde-format
msgid "Startup Services"
msgstr "ჩატვირთვისას გაშვებული სერვისები"

#: ui/main.qml:135
#, kde-format
msgid "Load-on-Demand Services"
msgstr "საჭიროებისამებრ ჩატვირთვადი სერვისები"

#: ui/main.qml:152
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr "სერვისის ჩატვირთვისას გაშვების გადართვა"

#: ui/main.qml:219
#, kde-format
msgid "Not running"
msgstr "გაშვებული არაა"

#: ui/main.qml:220
#, kde-format
msgid "Running"
msgstr "გაშვებული"

#: ui/main.qml:238
#, kde-format
msgid "Stop Service"
msgstr "სერვისის გაჩერება"

#: ui/main.qml:238
#, kde-format
msgid "Start Service"
msgstr "სერვისის გაშვება"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Temuri Doghonadze"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "Temuri.doghonadze@gmail.com"

#~ msgid "Background Services"
#~ msgstr "ფონური სერვისები"

#~ msgid "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"
#~ msgstr "(c) 2002 Daniel Molkentin, (c) 2020 Kai Uwe Broulik"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "კაი უვე ბროულიკი"
