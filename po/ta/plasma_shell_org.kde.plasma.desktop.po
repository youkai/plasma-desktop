# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Kishore G <kishore96@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-26 02:00+0000\n"
"PO-Revision-Date: 2023-05-21 14:37+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "இப்போது பயன்படுத்தப்படுவது"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"இந்த செயல்பாட்டிற்கு\n"
"நகர்த்து"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"இந்த செயல்பாட்டிலும்\n"
"காட்டு"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "அமை"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "செயல்பாட்டை நிறுத்து"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "நிறுத்திய செயல்பாட்டுகள்:"

#: contents/activitymanager/ActivityManager.qml:120
msgid "Create activity…"
msgstr "செயல்பாட்டை உருவாக்கு"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "செயல்பாடுகள்"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "செயல்பாட்டை அமை"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "நீக்கு"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "மன்னிக்கவும்! %1 என்பதை ஏற்றுவதில் சிக்கல் ஏற்பட்டது."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "பிடிப்புப்பலகைக்கு நகலெடு"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "சிக்கலின் விவரங்களை காட்டு…"

#: contents/applet/CompactApplet.qml:74
msgid "Open %1"
msgstr "%1-ஐ திற"

#: contents/configuration/AboutPlugin.qml:20
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr "பற்றி"

#: contents/configuration/AboutPlugin.qml:48
msgid "Send an email to %1"
msgstr "%1 என்பவருக்கு மின்னஞ்சல் அனுப்பு"

#: contents/configuration/AboutPlugin.qml:62
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "%1 எனும் இணையதளத்தைத் திற"

#: contents/configuration/AboutPlugin.qml:130
msgid "Copyright"
msgstr "பதிப்புரிமை"

#: contents/configuration/AboutPlugin.qml:148 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "உரிமம்:"

#: contents/configuration/AboutPlugin.qml:151
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "உரிமத்தின் உரையைக் காட்டும்"

#: contents/configuration/AboutPlugin.qml:165
msgid "Authors"
msgstr "இயற்றியவர்கள்"

#: contents/configuration/AboutPlugin.qml:176
msgid "Credits"
msgstr "நன்றி"

#: contents/configuration/AboutPlugin.qml:188
msgid "Translators"
msgstr "மொழிபெயர்ப்பாளர்கள்"

#: contents/configuration/AboutPlugin.qml:205
msgid "Report a Bug…"
msgstr "பிழையை தெரிவி…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "விசைப்பலகை சுருக்குவழிகள்"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "அமைப்புகளை செயல்படுத்து"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"தற்போதைய கூறின் அமைப்புகள் மாறியுள்ளன. அவற்றை செயல்படுத்த வேண்டுமா, அல்லது கைவிட "
"வேண்டுமா?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "சரி"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "செயல்படுத்து"

#: contents/configuration/AppletConfiguration.qml:338
msgid "Cancel"
msgstr "ரத்து செய்"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "விருப்பத்தேர்வு பக்கத்தை திற"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "இடது பட்டன்"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "வலது பட்டன்"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "நடு பட்டன்"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "பின் பட்டன்"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "முன் பட்டன்"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "செங்குத்து உருளல்"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "கிடைமட்ட உருளல்"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:98
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:170
msgctxt "@title"
msgid "About"
msgstr "பற்றி"

#: contents/configuration/ConfigurationContainmentActions.qml:185
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "செயலை சேர்"

#: contents/configuration/ConfigurationContainmentAppearance.qml:67
msgid "Layout changes have been restricted by the system administrator"
msgstr "தளவமைப்பு மாற்றங்களை கணினி நிர்வாகி தடைசெய்துள்ளார்"

#: contents/configuration/ConfigurationContainmentAppearance.qml:82
msgid "Layout:"
msgstr "தளவமைப்பு:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:96
msgid "Wallpaper type:"
msgstr "பின்புல படத்தின் வகை:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:116
msgid "Get New Plugins…"
msgstr "புதிய செருகுநிரல்களை பதிவிறக்கு…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:184
msgid "Layout changes must be applied before other changes can be made"
msgstr "தளவமைப்பை மாற்றியதை செயல்படுத்திய பின்னரே மற்ற மாற்றங்களை நீங்கள் செய்யலாம்"

#: contents/configuration/ConfigurationContainmentAppearance.qml:188
msgid "Apply Now"
msgstr "இப்போது செயல்படுத்து"

#: contents/configuration/ConfigurationShortcuts.qml:17
msgid "Shortcuts"
msgstr "சுருக்குவழிகள்"

#: contents/configuration/ConfigurationShortcuts.qml:29
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "நீங்கள் பிளாஸ்மாய்டை க்ளிக் செய்த‍து போலவே இச்சுருக்குவழி அதனை இயக்கும்."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "பின்புல படம்"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "சுட்டி செயல்கள்"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "இங்கு உள்ளிடவும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:43
msgid "Panel Settings"
msgstr "பலகைக்கான அமைப்புகள்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:49
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "அதிகபட்ச பெரிதாக்கு"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:53
msgid "Make this panel as tall as possible"
msgstr "பலகையை முடிந்தளவு நீளமாக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as wide as possible"
msgstr "பலகையை முடிந்தளவு நீளமாக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:62
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "நீக்கு"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:65
msgid "Remove this panel; this action is undo-able"
msgstr "பலகையை நீக்கும்; இச்செயலை செயல்நீக்கலாம்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:89
msgid "Alignment:"
msgstr "சீரமைப்பு:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Top"
msgstr "மேலே"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:95
msgid "Left"
msgstr "இடதுபுறமாக"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை மேல் ஓரத்துடன் சீரமைக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:96
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை மேல் ஓரத்துடன் சீரமைக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:104
msgid "Center"
msgstr "நடுவில்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:105
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை நடுப்படுத்தும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Bottom"
msgstr "கீழே"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:113
msgid "Right"
msgstr "வலதுபுறமாக"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை கீழ் ஓரத்துடன் சீரமைக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை வலது ஓரத்துடன் சீரமைக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:142
msgid "Visibility:"
msgstr "காட்டப்படுவது:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:147
msgid "Always Visible"
msgstr "எப்போது காட்டப்படும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Auto-Hide"
msgstr "தானாக மறை"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:156
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""
"பலகை இயல்பாக மறைந்திருக்கும். திரையின் ஓரத்தில் சுட்டிக்குறியை வைக்கும்போது பலகை "
"காட்டப்படும்."

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:183
msgid "Opacity:"
msgstr "ஒளிபுகாமை:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:188
msgid "Always Opaque"
msgstr "எப்போதும் ஒளிபுகாத‍து"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:196
msgid "Adaptive"
msgstr "மாறக்கூடியது"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:197
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""
"பலகையை சாளரங்கள் தொடும்போது பலகை ஒளிபுகாத‍தாக இருக்கும். மற்ற நேரங்களில் ஒளிபுகுவதாக "
"இருக்கும்."

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Always Translucent"
msgstr "எப்போதும் ஒளிபுகக்கூடியது"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:233
msgid "Floating:"
msgstr "மிதப்பது:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:238
msgid "Floating"
msgstr "மிதப்பது"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:239
msgid "Panel visibly floats away from its screen edge"
msgstr "திரையின் ஓரத்ததை தொடாமல் பலகை மிதக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:248
msgid "Attached"
msgstr "ஒட்டுவது"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:249
msgid "Panel is attached to its screen edge"
msgstr "திரையின் ஓரத்துடன் பலகை ஒட்டியிருக்கும்"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:271
msgid "Focus Shortcut:"
msgstr "குவியப்படுத்தும் சுருக்குவழி:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:281
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "பலகையை குவியப்படுத்த இந்த விசைச் சுருக்குவழியை அழுத்துங்கள்"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "அதிகபட்ச உயரத்தை மாற்ற இழுங்கள்."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "அதிகபட்ச அகலத்தை மாற்ற இழுங்கள்."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "மீட்டமைக்க இரட்டைக் கிளிக் செய்யுங்கள்."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "குறைந்தபட்ச உயரத்தை மாற்ற இழுங்கள்."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "குறைந்தபட்ச அகலத்தை மாற்ற இழுங்கள்."

#: contents/configuration/panelconfiguration/Ruler.qml:65
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"திரையின் இவ்வோரத்தில் இருப்பிடத்தை மாற்ற இழுங்கள்.\n"
"மீட்டமைக்க இரட்டைக் கிளிக் செய்யுங்கள்."

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "Add Widgets…"
msgstr "பிளாஸ்மாய்டுகளை சேர்…"

#: contents/configuration/panelconfiguration/ToolBar.qml:27
msgid "Add Spacer"
msgstr "இடைவெளியை சேர்"

#: contents/configuration/panelconfiguration/ToolBar.qml:28
msgid "More Options…"
msgstr "மேலும் விருப்பங்கள்…"

#: contents/configuration/panelconfiguration/ToolBar.qml:226
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "நகர்த்த இழுங்கள்"

#: contents/configuration/panelconfiguration/ToolBar.qml:265
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "அம்புக்குறி விசைகளால் பலகையை நகர்த்துங்கள்"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel width:"
msgstr "பலகையின் அகலம்:"

#: contents/configuration/panelconfiguration/ToolBar.qml:286
msgid "Panel height:"
msgstr "பலகையின் உயரம்:"

#: contents/configuration/panelconfiguration/ToolBar.qml:406
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "மூடு"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "பலகைகளும் பணிமேடை நிர்வாகமும்"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "பலகைகளையும் பணிமேடைகளையும் இழுத்து அவற்றை வெவ்வேறு திரைகளுக்கு நகர்த்தலாம்."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "%1 என்ற திரையிலுள்ள பணிமேடையுடன் இடமாற்று"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "%1 என்ற திரைக்கு நகர்த்து"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "பணிமேடையை நீக்கு"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "பலகையை நீக்கு"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (முதன்மையானது)"

#: contents/explorer/AppletAlternatives.qml:64
msgid "Alternative Widgets"
msgstr "மாற்று பிளாஸ்மாய்டுகள்"

#: contents/explorer/AppletDelegate.qml:167
msgid "Undo uninstall"
msgstr "நிறுவல்நீக்கியதை செயல்நீக்கு"

#: contents/explorer/AppletDelegate.qml:168
msgid "Uninstall widget"
msgstr "பிளாஸ்மாய்டை நிறுவல்நீக்கு"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "இயற்றியவர்:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "மின்னஞ்சல்:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "நிறுவல் நீக்கு"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "அனைத்து பிளாஸ்மாய்டுகள்"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "பிளாஸ்மாய்டுகள்"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "புதிய பிளாஸ்மாய்டுகளை பெறு…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "வகைகள்"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets matched the search terms"
msgstr "தேடலுக்கு எந்த பிளாஸ்மாய்டும் பொருந்தவில்லை"

#: contents/explorer/WidgetExplorer.qml:283
msgid "No widgets available"
msgstr "பிளாஸ்மாய்டுகள் ஏதுமில்லை"

#~ msgid "Switch"
#~ msgstr "மாற்று"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Above"
#~ msgstr "சாளரங்கள் பின்னே"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Below"
#~ msgstr "சாளரங்கள் பின்னே"

#, fuzzy
#~| msgid ""
#~| "Makes the panel remain visible always but part of the maximized windows "
#~| "shall go below the panel as though the panel did not exist."
#~ msgid ""
#~ "Like \"Always Visible\", but maximized and tiled windows go under the "
#~ "panel as though it didn't exist"
#~ msgstr ""
#~ "பலகை எப்போதும் காட்டப்படும், ஆனால் அதிகபட்ச பெரிதாக்கப்பட்டுள்ள சாளரங்கள் அதற்கு அடியில் "
#~ "செல்ல‍லாம்."

#~ msgid "Windows In Front"
#~ msgstr "சாளரங்கள் முன்னே"

#~ msgid "Aligns the panel"
#~ msgstr "பலகையை சீரமைக்கும்"

#~ msgid "Center aligns the panel if the panel is not maximized."
#~ msgstr "பலகை அதிகபட்ச நீளத்தைக் கொண்டிராத பட்சத்தில் அதை நடுப்படுத்தும்."

#~ msgid ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."
#~ msgstr ""
#~ "பலகை இயல்பாக மறைக்கப்பட்டிருக்கும். பலகை இருக்கவேண்டிய இடத்தில் சுட்டிக்குறி "
#~ "நுழையும்போது பலகையைக் காட்டும்"

#~ msgid ""
#~ "Makes the panel remain visible always but maximized windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."
#~ msgstr ""
#~ "பலகை எப்போதும் காட்டப்படும், ஆனால் சாளரங்கள் அதை மறைக்கலாம். பலகை இருக்கவேண்டிய "
#~ "இடத்தில் சுட்டிக்குறி நுழையும்போது பலகை காட்டப்படும்."

#~ msgid "Makes the panel translucent except when some windows touch it."
#~ msgstr "பலகையை சாளரங்கள் தொடாத பட்சத்தில் ஒளிபுகுவதாக இருக்கும்."

#~ msgid "Makes the panel translucent always."
#~ msgstr "பலகை எப்போதும் ஒளிபுகுவதாக இருக்கும்."

#~ msgid "Makes the panel float from the edge of the screen."
#~ msgstr "பலகையை திரையின் ஓரத்தைத் தொடாமல் மிதக்க வைக்கும்."

#~ msgid "Makes the panel remain attached to the edge of the screen."
#~ msgstr "பலகையை திரையின் ஓரத்துடன் இணைந்துள்ளவாறு அமைக்கும்."

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (முடக்கப்பட்டுள்ளது)"

#~ msgid "Appearance"
#~ msgstr "தோற்றம்"

#~ msgid "Search…"
#~ msgstr "தேடு..."

#~ msgid "Screen Edge"
#~ msgstr "திரையின் ஓரம்"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr "பலகையை வேறொரு ஓரத்திற்கு நகர்த்த இப்பட்டனை அழுத்தி இழுக்கவும்."
