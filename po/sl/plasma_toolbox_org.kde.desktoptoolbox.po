# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrej Mernik <andrejm@ubuntu.si>, 2014, 2015.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-12 02:01+0000\n"
"PO-Revision-Date: 2023-01-07 09:48+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 22.12.0\n"

#: contents/ui/ToolBoxContent.qml:266
#, kde-format
msgid "Choose Global Theme…"
msgstr "Izberi globalno temo…"

#: contents/ui/ToolBoxContent.qml:273
#, kde-format
msgid "Configure Display Settings…"
msgstr "Postavi nastavitve prikaza…"

#: contents/ui/ToolBoxContent.qml:294
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Več"

#: contents/ui/ToolBoxContent.qml:309
#, kde-format
msgid "Exit Edit Mode"
msgstr "Izhod iz urejevalnega načina"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Končaj prilagoditev postavitve"

#~ msgid "Color:"
#~ msgstr "Barva:"

#~ msgid "Select Background Color"
#~ msgstr "Izberi barvo ozadja"

#~ msgid "Lock Screen"
#~ msgstr "Zakleni zaslon"

#~ msgid "Leave"
#~ msgstr "Zapusti"
